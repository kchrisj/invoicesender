# invoiceSender
First part of a four part system:
1. Send current invoice via email to P.I. (Invoice Sender)
2. P.I. can view details of current invoice via a web page (invoiceDetialsVue)
3. Send overdue invoice via email to P.I. (latePay)
4. P.I. can view details of over due invoice via a web page (latePayInvoiceDetailsVue)


This is part one -- send email with invoice as an attachment to P.I.

It uses and array of invoice numbers
Main files:
1.  invoiceSender/Apps/
    --> start.php --> triggers the InvoiceSender class
        Note an array of invoice numbers is passed to the class

2.  invoiceSender/Apps/
    --> InvoiceSender.php does the work
    --> EquipmentUseDAO.php --> gather info of equipment used
    --> EmailMessageGenerator --> create email message with equipment information. Uses twig templates.
    --> EmailMessageSender --> send email with attach, to P.I.

3.  invoiceSender/Apps/
    -->InvoiceDetaits.php --> sends email to P.I.

4.  invoiceSender/data
    --> contains logfiles and dummy invoices

5.  InvoiceSender/config
    --> config.ini --> info for database and email
