<?php
namespace Apps;

class InvoiceSender
{

  /**
  * Collect information on equipment used via
  * array of invoice numbers
  */

  static function sendInvoice($connection, $invoiceIdArray){
    foreach($invoiceIdArray as $invoiceId){
        try {
            $useDetailsObject = EquipmentUseDAO::getEquipmentUseDetails($connection, $invoiceId);
            //print_r($useDetailsObject);
        } catch (Exception $e) {
            $log->error($e->getMessage());
            echo "Problem retrieving data from database\r\n";
            die();
        }
        try{
            //print_r($detailsObject);
            $emailMessage = EmailMessageGenerator::createEmail($useDetailsObject);
        } catch(Exception $e){
            $log->error($e->getMessage());
            echo "Could not generate email message\r\n";
            die();
        }
        try{
             EmailMessageSender::sendEmail($emailMessage);
        } catch(Exception $e){
            $log->error($e->getMessage());
            echo "Could not send email message\r\n";
            die();
        }
    }
  }
}
