<?php

namespace Apps;

class EquipmentUseDAO
{
  /**
  * Data Access Object
  * Because static methods are callable without an instance of the object created,
  * the pseudo-variable $this is not available inside the method declared as static.
  
  * Collect information on equipment used
  *
  */

  function __construc()
  {

  }

  static function getEquipmentUseDetails($dbConnection, $invoiceId)
  {
    $allDetails = [];
    $useDetailsObject = new EquipmentUseDetails();

    $query = "SELECT CII.invoice, CI.issued, CI.due, CS.name, CII.service_time, CII.quantity, CII.rate, CII.total, CI.filename, CI.payer, P.email, concat_ws(' ', P.first_name, P.last_name) AS name, access_code
        FROM core_invoice_item AS CII, core_invoice AS CI, people AS P, core_services AS CS
        WHERE CII.invoice = ? AND CII.invoice = CI.number AND P.individual_id = CI.payer AND CS.id = CII.service";

    if($results = $dbConnection->prepare($query)){
      $results->bind_param("s", $invoiceId);
      $results->execute();
      /* bind result variables */
      $results->bind_result($invoice, $issued, $due, $service_name, $service_time, $quantity, $rate, $total, $filename, $payer, $email, $name, $access_code);
      //var_dump($results);

      /* fetch values */
       while($row = $results->fetch()){
        if(!$useDetailsObject->invoiceNumber){
          $useDetailsObject->fileName = $filename;
          $useDetailsObject->email = $email;
          $useDetailsObject->userName = $name;
          $useDetailsObject->dueDate = $due;
          $useDetailsObject->accessCode = $access_code;

          $useDetailsObject->serviceInfoArray = [];
          array_push($useDetailsObject->serviceInfoArray, $invoice, $issued, $due, $service_name, $service_time, $quantity, $rate, $total, $payer);
          if(!in_array($invoice, $useDetailsObject->invoiceDetailsArray)){
            array_push($useDetailsObject->invoiceDetailsArray, $useDetailsObject->serviceInfoArray);
          }
          if(!in_array($filename, $useDetailsObject->attachmentArray)){
            array_push($useDetailsObject->attachmentArray, $filename);
          }
        }elseif($useDetailsObject->invoiceNumber == $invoice) {
          array_push($useDetailsObject->invoiceDetailsArray, $useDetailsObject->serviceInfoArray);
        }
      } //endWhile
    }

    return($useDetailsObject);
  }

}


/*SELECT CII.invoice, CI.issued, CI.due, CS.name, CII.service_time, CII.quantity, CII.rate, CII.total, CI.filename, CI.payer, P.email, concat_ws(' ', P.first_name, P.last_name) AS name
    FROM core_invoice_item AS CII, core_invoice AS CI, people AS P, core_services AS CS
    WHERE CII.invoice = "A00001" AND CII.invoice = CI.number AND P.individual_id = CI.payer AND CS.id = CII.service
*/
